---
sidebar_position: 3
---

import Tabs from '@theme/Tabs';
import TabItem from '@theme/TabItem';
import Quiz from '@site/src/components/Quiz';

# Netwerken, Internet en het World Wide Web

In deze leeractiviteit leer je over [**netwerken**](#netwerken), [**het internet**](#het-internet) en het [**World Wide Web**](#world-wide-web). **Full-stack applicaties** zijn applicaties die uit verschillende onderdelen bestaan (bijvoorbeeld een *front-end-* en een *backend*-gedeelte). Deze onderdelen wisselen gegevens uit via een netwerkverbinding, meestal via het internet. Op die manier kunnen gebruikers toegang verkrijgen tot een centrale databron en/of onderling gegevens uitwisselen. Om die reden is het als **front-end-**, **backend-** of **full-stack developer** belangrijk om kennis te hebben van deze onderwerpen. 

### Sleutelwoorden

> Networking, Internet, World-Wide Web, Protocols, Internet Protocol (IP), Transmission Control Protocol (TCP), User Datagram Protocol (UDP), Hypertext Transfer Protocol (HTTP), Application Programming Interface (API), Domain Name Service (DNS), Webservices 

## Netwerken

Computers kunnen met elkaar verbonden worden via kabels of Wi-Fi. Het OSI-netwerkmodel is een conceptueel model dat vaak gebruikt wordt om de verschillende lagen van een computernetwerk van elkaar te onderscheiden.

![OSI Netwerkmodel](/img/osi.png)

[^1]: Bron: https://www.helpnetsecurity.com/2019/05/23/connected-devices-growth/.

Dit model beschrijft (van beneden naar boven) de volgende lagen: 

1. **Fysieke laag:** fysieke hardware, zoals bekabeling of Wi-Fi. 
2. **Link-laag:** brengt verbinding tussen computers tot stand op de fysieke laag (*Ethernet*).
3. **Netwerklaag:** verstuurt en tussen verschillende computers in een gedecentraliseerd netwerk (via *Internet Protocol* (IP)).
4. **Transportlaag:** aanvullende logica op netwerklaag (bijv. ontvangstbevestiging met TCP/IP)  
7. **Applicatielaag:** de laag waarop applicaties met elkaar communiceren (bijv. het *World-Wide-Web*, laag 5 t/m 7)

:::note

De bovenste drie lagen (5, 6 en 7) zijn meestal samengevoegd in de applicatielaag. Voor applicatieontwikkelaars zijn met name de bovenste lagen (transport- en applicatielaag) van belang omdat dit de laag is waar onze code direct mee communiceert (de onderliggende lagen zijn weggeabstraheerd).

:::

### 📖 Kijk en lees: Netwerken
1. [Computer Networking in 100 Seconds](https://www.youtube.com/watch?v=keeqnciDVOo)
2. [Crash Course Computer Science - Computer Networks (YouTube)](https://www.youtube.com/watch?v=3QhU9jd03a0)
3. [Computernetwerk (Wikipedia)](https://nl.wikipedia.org/wiki/Computernetwerk)

### ⌨️ Oefeningen: Netwerken

1. Voer het volgende commando in in de terminal. Kun je hier het IP-adres vinden van jouw computer op je lokale netwerk? (bijv. `192.168.0.2`)

<Tabs>
  <TabItem value="windows" label="Windows" default>

```
ipconfig /all
``` 

  </TabItem>
  <TabItem value="mac/linux" label="MacOS / Linux">

```bash
ifconfig
``` 

  </TabItem>
</Tabs>

## Het Internet

Het grootste computernetwerk ter wereld is het internet. Dit is een wereldwijd netwerk van computers die met elkaar in verbinding staan en gegevens uit kunnen wisselen via het **Internet Protocol** (IP). Dit is de netwerklaag van het [OSI-netwerkmodel](#netwerken), waarop computers data uitwisselen in kleine pakketjes.

Het internet is een *gedecentraliseerd* netwerk, wat inhoudt dat er geen centraal punt is waar alle data doorheen gaat. Dit maakt het netwerk zeer betrouwbaar; als er ergens in het netwerk een computer uitvalt kan een datapakketje via een andere route op haar bestemming komen.

![Het internet als gedecentraliseerd netwerk](/img/internet_topology.png)

:::info

In 2019 waren er naar schatting 22 biljoen(!) apparaten via het internet verbonden[^1]. 

[^1]: [bron](https://www.helpnetsecurity.com/2019/05/23/connected-devices-growth/).

:::

### 📖 Kijk en lees: Het Internet
1. [How does the Internet work?](https://roadmap.sh/guides/what-is-internet)
2. [Crash Course Computer Science - The Internet (YouTube)](https://www.youtube.com/watch?v=AEaKrq3SpW8)
3. [Het Internet (Wikipedia)](https://nl.wikipedia.org/wiki/Internet)
4. [DNS Explained in 100 seconds (YouTube)](https://www.youtube.com/watch?v=UVR9lhUGAyU)

### ⌨️ Oefeningen: Het Internet

1. Maak verbinding met de server van Google via de utility [`ping`](https://en.wikipedia.org/wiki/Ping_(networking_utility)), door in de command-line onderstaand commando in te typen. Wat zie je?

```bash
ping www.google.com
``` 

2. Bekijk met de command-line utility [`traceroute/tracert`](https://en.wikipedia.org/wiki/Traceroute) via welke stappen (*hops*) een gegevens van jouw computer naar de server van Google verstuurd worden. Hoeveel tussenliggende stappen zijn er? Wat kun je hieruit opmaken?

<Tabs>
  <TabItem value="windows" label="Windows" default>

```
tracert www.google.com
``` 

  </TabItem>
  <TabItem value="mac/linux" label="MacOS / Linux">

```bash
traceroute www.google.com
``` 

  </TabItem>
  <TabItem value="powershell" label="Powershell (Windows)">

```powershell
Test-NetConnection -TraceRoute www.google.com
``` 

  </TabItem>
</Tabs>

### 💡 Quiz: Het Internet

<Quiz quiz={{
    question: "Uit hoeveel cijfers bestaat een (IPv4) IP-adres maximaal?",
    answers: [
        "6",
        "9",
        "12"
    ],
    correctAnswer: 2
}} />

## World Wide Web

De begrippen 'internet' en '**World Wide Web**' (WWW) worden vaak door elkaar gebruikt, maar ze zijn strikt genomen niet hetzelfde. Het World Wide Web is een specifieke toepassing op het internet, naast bijvoorbeeld e-mail en VoIP (internetbellen). Het World Wide Web bestaat uit een groot aantal verschillende protocollen en technologieën, zoals het Hypertext Markup Language (HTML), Cascading Style Sheets (CSS) en Hypertext Transfer Protocol (HTTP). Zie ook de leeractiviteit [Front-end Development](#).

### 📖 Kijk en lees: World Wide Web 
1. [Web Demystified (YouTube)](https://www.youtube.com/playlist?list=PLo3w8EB99pqLEopnunz-dOOBJ8t-Wgt2g)
2. [Crash Course Computer Science - The World Wide Web The World Wide Web (YouTube)](https://www.youtube.com/watch?v=guvsH5OFizE)
3. [Wereldwijd Web (Wikipedia)](https://nl.wikipedia.org/wiki/Wereldwijd_web)
3. [HTTP (MDN Web Docs)](https://developer.mozilla.org/en-US/docs/Web/HTTP)

### ⌨️ Oefeningen: World Wide Web

1. Navigeer naar de URL https://www.google.com in je browser en open de [*Developer Console*](https://balsamiq.com/support/faqs/browserconsole/). Navigeer naar de tab Netwerk/Network. Welke informatie zie je hier?

2. Maak een HTTP-request naar de server van Google met de command-line utility [`curl`](https://curl.se/). Wat zie je?

```bash
curl www.google.com
``` 

3. Maak een HTTP-request in code en print het resultaat naar de console. Wat zie je?

<Tabs>
  <TabItem value="java" label="Java" default>

```java
URL googleURL = new URL("https://www.google.com");
URLConnection googleURLConnection = googleURL.openConnection();
BufferedReader in = new BufferedReader(new InputStreamReader(googleURLConnection.getInputStream()));
String inputLine;

while ((inputLine = in.readLine()) != null) {
    System.out.println(inputLine);
}
in.close();
``` 

  </TabItem>
  <TabItem value="javascript" label="Javascript">

```js
fetch("https://www.google.com").then((result) => {
    console.log(result);
}
``` 

  </TabItem>
</Tabs>

4. Clone het repository[^2] [Java-HTTP-Client](https://gitlab.com/adsd-hu/java-http-client/) van de AdSD Gitlab, bekijk de code en voer het programma uit in je IDE. Wat zie je?

[^2]: Voor instructies t.a.v. het clonen van een repository, zie [Werken met Git](#)


### 💡 Quiz: World Wide Web 

<Quiz quiz={{
    question: "Bij welke OSI-netwerklaag hoort het HTTP-protocol?",
    answers: [
        "De Transportlaag",
        "De Applicatielaag",
        "De Link-laag"
    ],
    correctAnswer: 1
}} />