import React, { useState } from "react";
import "./Quiz.css";

const alphabet = ["A", "B", "C", "D", "E", "F", "G"];

const Quiz = ({ quiz }) => {
  const [selectedAnswer, setSelectedAnswer] = useState();
  const [isCorrect, setIsCorrect] = useState<boolean | undefined>();

  const handleSelection = (index) => {
    setSelectedAnswer(index);
  };

  const handleSubmit = () => {
    if (selectedAnswer === undefined) return;
    setIsCorrect(selectedAnswer === quiz.correctAnswer);
  };

  return (
    <div className="quiz">
      <div className="question">
        <div className="question-text">{quiz.question}</div>
      </div>
      <div className="answers">
        {quiz.answers.map((answer: string, index: number) => (
          <div
            className={
              index === selectedAnswer ? "answer-text-selected" : "answer-text"
            }
            onClick={() => handleSelection(index)}
          >
            {alphabet[index]}. {answer}
          </div>
        ))}
      </div>
      <div className="answer-section">
        <button className="submit-button" onClick={handleSubmit}>
          Antwoord
        </button>
        {isCorrect !== undefined &&
          (isCorrect ? (
            <div className="correct-text">Dat is goed, gefeliciteerd!</div>
          ) : (
            <div className="incorrect-text">
              Dat is helaas fout, probeer opnieuw.
            </div>
          ))}{" "}
      </div>
    </div>
  );
};

export default Quiz;
